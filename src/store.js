import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    isLandscape: true,
    towns: [
      {
        id: 1,
        name: 'Business Owner',
        character: 'chr-businessowner.png',
        descImage: 'desc-businessowner.png',
        completed: false,
        description: 'Businesses in PineVille are mostly small family- owned types. They include mostly small bakeries, grocery stores and car repair shops. Most of them are content with having a small group of clients within the neighbourhood, but some do wish for more opportunities to earn more income. They believe in earning an honest living by charging reasonable prices and building positive relationships with customers.',
        member: 'On good days, we earn a decent amount – just enough to feed the family. It is always the same few customers. It is nice that we all know one another, and everyone is happy. But on bad days, when the weather is terrible for example, we hardly earn enough to cover the expenses. It can get quite tough owning a business here, but since we own the shop, we don\'t have to worry about rent.',
        quiz: {
          question: 'Include in your speech to the business-owners, the ways a shopping centre will improve their business. Emphasise that...',
          choices: [
            {
              name: 'Neutral',
              type: 1,
              text: 'they will enjoy a 20% discount on rent for the first year.',
              score: 0,
              final: 0,
              speech: 'You will enjoy 20% discount on rent for your first year!',
              audio: require(`@/assets/audio/speech/business-neutral.mp3`)
            },
            {
              name: 'Bad',
              type: 0,
              text: 'a part of the profits earned will need to go into the fund to maintain the shopping centre.',
              score: -1,
              final: 0,
              speech: 'Remember, a part of your profits earned will go into the fund to maintain the shopping centre so it will always be kept new',
              audio: require(`@/assets/audio/speech/business-bad.mp3`)
            },
            {
              name: 'Good',
              type: 2,
              text: 'they will enjoy more human traffic as it attracts people from all over town. The management will also advertise the business at no extra cost.',
              score: 1,
              final: 1,
              speech: 'You will enjoy having more visitors as the shopping centre will be a big attraction to people all over town!',
              audio: require(`@/assets/audio/speech/business-good.mp3`)
            }
          ]
        }
      },
      {
        id: 2,
        name: 'Farmer',
        character: 'chr-farmer.png',
        descImage: 'desc-farmer.png',
        completed: false,
        description: 'The farmers in PineVille have been around since the beginning. The farms are largely family-owned and have been passed down from generation to generation. They take pride in producing honest products for the residents of PineVille. They believe the old way of farming is the best way of farming, and have adopted very little technology in their processes. They are very generous in sharing their knowledge with the local schools and children, and believe that children need to know where the food they eat come from.',
        member: 'The best part of being a farmer is seeing how much your customers enjoy your product. The other part is seeing how children really want to learn how we do things here. It is hard work, and we are not getting any younger... the only problem is, the younger people don’t really want to be farmers. That is worrying for us!',
        quiz: {
          question: 'Include in your speech to the farmers, the ways a rail system will improve lives of locals. Emphasise that...',
          choices: [
            {
              name: 'Bad',
              type: 0,
              text: 'the rail network will cut through roughly 40% of farmland.',
              score: -1,
              final: 0,
              speech: 'This means the rail network will cut through roughly 40% of farmland. But don’t worry.',
              audio: require(`@/assets/audio/speech/farmer-bad.mp3`)
            },
            {
              name: 'Neutral',
              type: 1,
              text: 'the rail network will bring more visitors to the farm and hence they will enjoy more profits.',
              score: 0,
              final: 0,
              speech: 'It will bring more visitors and hence, customers to your farm! This means more profits!',
              audio: require(`@/assets/audio/speech/farmer-neutral.mp3`)
            },
            {
              name: 'Good',
              type: 2,
              text: 'the railway company will be investing in programmes to bring children on educational tours at the farm.',
              score: 1,
              final: 1,
              speech: 'The rail way company is investing 3 million dollars to design programmes, such as educational tours at the farm for children.',
              audio: require(`@/assets/audio/speech/farmer-good.mp3`)
            }
          ]
        }
      },
      {
        id: 3,
        name: 'Nature Lover',
        character: 'chr-naturelover.png',
        descImage: 'desc-naturelover.png',
        completed: false,
        description: 'The Nature Society in PineVille aims to protect the environment surrounding the town. In the past, they have vehemently opposed projects which involve cutting down of trees, or the building of roads and rail networks which are not seen as environmentally-friendly. Most residents tend to take to the Nature Society’s suggestions, indicating that they have a strong foothold in society. They also frequently publish opinion pieces in the local paper advocating the importance of green living.',
        member: 'We love PineVille for its greenery all around, it is the reason why we chose to raise Mike here. We do a small trek everyday around the hills, and go a little further during the weekends. Sometimes, we even camp out. We don’t need anything else really, and would hate buildings to replace this beautiful nature. Just leave us (and the trees) alone!',
        quiz: {
          question: 'Include in your speech to the nature-lovers, the ways in which a rail network will benefit the community. Emphasise that...',
          choices: [
            {
              name: 'Good',
              type: 2,
              text: 'it aims to connect the four major national parks in town so that residents can easily go from one park to another.',
              score: 1,
              final: 1,
              speech: 'The rail network will connect the four major national parks in PineVille so you can go from one park to another easily!',
              audio: require(`@/assets/audio/speech/nature-good.mp3`)
            },
            {
              name: 'Neutral',
              type: 1,
              text: 'it will be easier for the child to go to school in the near future.',
              score: 0,
              final: 0,
              speech: 'The rail network will mean your children can go to school much easier and much faster!',
              audio: require(`@/assets/audio/speech/nature-neutral.mp3`)
            },
            {
              name: 'Bad',
              type: 0,
              text: 'it will bring residents closer to nature as the train will cut through lush parts of PineVille’s forests.',
              score: -1,
              final: 0,
              speech: 'The rail network will bring all of us closer to nature as the train cuts through lush parts of PineVille’s forests, offering us a closer look at nature!',
              audio: require(`@/assets/audio/speech/nature-bad.mp3`)
            }
          ]
        }
      },
      {
        id: 4,
        name: 'Resident',
        character: 'chr-resident.png',
        descImage: 'desc-resident.png',
        completed: false,
        description: 'The residents of PineVille prefer a quiet lifestyle and mostly reject policies which could potentially damage the environment. To them, sustainability is key. They are, however, excited to see some changes in their town, since they are always thinking of ways to improve their life in PineVille.',
        member: 'I hope for more shops in my town. I now have to travel 30 miles to a shop in another town that sells all my books and games. Sometimes, they don’t even have what I want! Sure, I like that it is quiet here, but I really just hope to have more choices and to get my stuff more easily!',
        quiz: {
          question: 'Include in your speech to the residents, the ways in which a shopping centre can help to serve their needs. Emphasise that the shopping centre will...',
          choices: [
            {
              name: 'Bad',
              type: 0,
              text: 'replace what is now Pinetree Park, a park popular with runners and cyclists in PineVille.',
              score: -1,
              final: 0,
              speech: 'The shopping centre will replace what is now Pinetree Park.',
              audio: require(`@/assets/audio/speech/homeowner-bad.mp3`)
            },
            {
              name: 'Good',
              type: 2,
              text: 'be built out of sustainable materials and house a total of 350 shops, catering to every person’s needs.',
              score: 1,
              final: 1,
              speech: 'The shopping centre will be built out of sustainable materials and house a total of 350 shops, catering to every one of your needs.',
              audio: require(`@/assets/audio/speech/homeowner-good.mp3`)
            },
            {
              name: 'Neutral',
              type: 1,
              text: 'be painted with the chosen colour of PineVille residents.',
              score: 0,
              final: 0,
              speech: 'The shopping centre will be painted with your choice of colour.',
              audio: require(`@/assets/audio/speech/homeowner-neutral.mp3`)
            }
          ]
        }
      }
    ],
    isMayorSelected: false,
    crowd: [
      {
        name: 'Bad',
        type: 0,
        image: null,
        video: require(`@/assets/videos/audience-angry.mp4`),
        audio: require(`@/assets/audio/speech/crowd/crowd-bad.mp3`)
      },
      {
        name: 'Neutral',
        type: 1,
        image: require(`@/assets/images/speech/audience-neutral.jpg`),
        audio: require(`@/assets/audio/speech/crowd/crowd-neutral.mp3`)
      },
      {
        name: 'Good',
        type: 2,
        image: null,
        video: require(`@/assets/videos/audience-happy.mp4`),
        audio: require(`@/assets/audio/speech/crowd/crowd-good.mp3`)
      }
    ],
    consequences: [
      {
        name: 'Good',
        title: 'The mayor\'s plan goes through',
        description: 'Thank you for your help! I am excited to begin my work in PineVille with the support from the residents.',
        threshold: 75
      },
      {
        name: 'Bad',
        title: 'The mayor\'s plan falls through',
        description: 'Oh! It seems like I need to work harder to convince this stubborn group of residents.',
        threshold: 0
      }
    ],
    selectedTown: null,
    speechIndex: 0,
    finalScore: 0,
    confirmAnswer: 0,
    selectedArraySpeechScene: null,
    arraySpeechScene: [],
    totalScore: 0
  },
  mutations: {
    isLandscape (state, status) {
      state.isLandscape = status
    },
    setSelectedScene (state, scene) {
      state.selectedTown = scene
    },
    setMayorSelected (state) {
      state.isMayorSelected = true
    },
    setTotalScore (state, final) {
      state.totalScore += final
    },
    incrementSpeechIndex (state) {
      state.speechIndex++
    },
    setSceneCompleted (state, scene) {
      const index = state.towns.indexOf(scene)
      state.towns[index].completed = true
    },
    setSceneUncompleted (state, scene) {
      const index = state.towns.indexOf(scene)
      state.towns[index].completed = false
    },
    computeScore (state, speech) {
      let score = 0

      if (speech.score < 0) {
        score = 0
      } else {
        score = speech.score
      }

      state.totalScore += score
    },
    resetSelectedScene (state) {
      state.selectedTown = null
      state.totalScore = 0
      state.speechIndex = 0
      state.finalScore = 0
      state.confirmAnswer = 0
      state.currentBackgroundMusicTime = 0
      state.selectedArraySpeechScene = null
      state.arraySpeechScene = []

      state.towns.forEach(scene => {
        scene.completed = false
      })
    },
    setSelectedSpeech (state, speech) {
      state.arraySpeechScene.push(speech)
    },
    setConfirmAnswer (state, id) {
      state.confirmAnswer = id
    }
  },
  getters: {
    scenesCompleted (state) {
      return state.towns.every(scene => scene.completed)
    }
  }
})
