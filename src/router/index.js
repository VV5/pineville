import Vue from 'vue'
import Router from 'vue-router'
import Copyright from '@/components/Copyright'
import Introduction from '@/components/Introduction'
import Town from '@/components/Town'
import Speech from '@/components/Speech'
import Consequence from '@/components/Consequence'
import Mayor from '@/components/Mayor'
import Resident from '@/components/Resident'
import Credits from '@/components/Credits'

Vue.use(Router)

export default new Router({
  base: window.location.pathname,
  routes: [
    {
      path: '/',
      name: 'Copyright',
      component: Copyright,
      meta: {
        title: 'Copyright'
      }
    },
    {
      path: '/introduction',
      name: 'Introduction',
      component: Introduction,
      meta: {
        title: 'Introduction'
      }
    },
    {
      path: '/town',
      name: 'Town',
      component: Town,
      meta: {
        title: 'Town'
      }
    },
    {
      path: '/speech',
      name: 'Speech',
      component: Speech,
      meta: {
        title: 'Speech'
      }
    },
    {
      path: '/mayor',
      name: 'Mayor',
      component: Mayor,
      meta: {
        title: 'Mayor'
      }
    },
    {
      path: '/resident',
      name: 'Resident',
      component: Resident,
      meta: {
        title: 'Resident'
      }
    },
    {
      path: '/consequence',
      name: 'Consequence',
      component: Consequence,
      meta: {
        title: 'Consequence'
      }
    },
    {
      path: '/credits',
      name: 'Credits',
      component: Credits,
      meta: {
        title: 'Credits'
      }
    }
  ]
})
