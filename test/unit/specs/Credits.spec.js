import { shallowMount } from '@vue/test-utils'
import Credits from '@/components/Credits'

describe('Credits', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Credits)
  })

  test('nothing to test here. Just creating this for future use', () => {
    expect(wrapper.isVueInstance()).toBe(true)
  })
})
