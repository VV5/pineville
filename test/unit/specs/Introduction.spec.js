import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Introduction from '@/components/Introduction'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Introduction', () => {
  let wrapper, store, state

  beforeEach(() => {
    state = {}

    store = new Vuex.Store({
      state
    })

    wrapper = shallowMount(Introduction, {
      store, localVue
    })
  })

  test('should open plans modal correctly', () => {
    wrapper.find('.start-button').trigger('click')

    expect(wrapper.vm.isActive).toBe(true)
  })
})
