import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Town from '@/components/Town'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Town', () => {
  let wrapper
  let store, state, mutations, getters
  let router

  beforeEach(() => {
    state = {
      towns: [
        { name: 'Business Owner', character: 'chr-businessowner.png' },
        { name: 'Farmer', character: 'chr-farmer.png' }
      ]
    }

    mutations = {
      setSelectedScene: jest.fn(),
      setSceneCompleted: jest.fn()
    }

    getters = {
      scenesCompleted: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations, getters
    })

    router = new VueRouter()

    wrapper = shallowMount(Town, {
      store,
      router,
      localVue,
      computed: {
        isMayorSelected: () => true
      }
    })
  })

  test('', () => {
    const $route = { path: '/resident' }

    wrapper.findAll('.scene-button').at(0).trigger('click')

    expect(mutations.setSelectedScene).toHaveBeenCalled()
    expect(wrapper.vm.$route.path).toBe($route.path)
  })

  test('should navigate to mayor when button is clicked', () => {
    const $route = {
      path: '/mayor'
    }

    wrapper.find('.goto-mayor').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
