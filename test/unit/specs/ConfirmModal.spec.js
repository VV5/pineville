import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import ConfirmModal from '@/components/ConfirmModal'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('ConfirmModal', () => {
  let wrapper, store, state, mutations, computed

  beforeEach(() => {
    state = {}

    mutations = {
      setSelectedSpeech: jest.fn(),
      setSceneCompleted: jest.fn(),
      computeScore: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations
    })

    computed = {
      quiz: jest.fn(),
      choices: jest.fn(),
      answer: jest.fn()
    }

    wrapper = shallowMount(ConfirmModal, {
      store, localVue, computed
    })
  })

  test('should close modal when modal background is clicked', () => {
    wrapper.find('.modal-background').trigger('click')

    wrapper.vm.$emit('closeModal')

    expect(wrapper.emitted().closeModal).toBeTruthy()
  })

  test('should navigate to town when button is clicked', () => {
    const $route = {
      path: '/town'
    }

    const router = new VueRouter()

    wrapper = shallowMount(ConfirmModal, {
      store, router, localVue, computed
    })

    wrapper.find('.check-answer').trigger('click')

    expect(mutations.setSelectedSpeech).toHaveBeenCalled()
    expect(mutations.setSceneCompleted).toHaveBeenCalled()
    expect(mutations.computeScore).toHaveBeenCalled()

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
