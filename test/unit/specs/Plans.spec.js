import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Plans from '@/components/Plans'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Plans', () => {
  let wrapper
  let router

  beforeEach(() => {
    router = new VueRouter()

    wrapper = shallowMount(Plans, {
      router, localVue
    })
  })

  test('should close the modal when modal background is clicked', () => {
    wrapper.find('.modal-background').trigger('click')

    expect(wrapper.emitted().closeModal).toBeTruthy()
  })

  test('should decrement planIndex when back button is clicked', () => {
    wrapper.setData({ planIndex: 2 })

    wrapper.findAll('.is-back').at(0).trigger('click')

    expect(wrapper.vm.planIndex).toBe(1)
  })

  test('should increment planIndex when next button is clicked', () => {
    expect(wrapper.vm.planIndex).toBe(0)

    wrapper.findAll('.is-next').at(0).trigger('click')

    expect(wrapper.vm.planIndex).toBe(1)
  })

  test('should navigate to town correctly when button is clicked', () => {
    const $route = { path: '/town' }

    wrapper.setData({ planIndex: wrapper.vm.plans.length - 1 })

    wrapper.findAll('.is-next').at(0).trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
