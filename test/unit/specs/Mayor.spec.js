import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Mayor from '@/components/Mayor'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Mayor', () => {
  let wrapper
  let store, state, mutations, getters
  let router

  beforeEach(() => {
    state = {}

    mutations = {
      setMayorSelected: jest.fn()
    }

    getters = {
      scenesCompleted: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations, getters
    })

    router = new VueRouter()

    wrapper = shallowMount(Mayor, {
      store,
      router,
      localVue,
      computed: {
        isSceneCompleted: jest.fn()
      }
    })
  })

  test('should navigate to speech when button is clicked', () => {
    const $route = {
      path: '/speech'
    }

    wrapper.find('button').trigger('click')

    wrapper.vm.goto(true)

    expect(mutations.setMayorSelected).toBeTruthy()
    expect(wrapper.vm.$route.path).toBe($route.path)
  })

  test('should navigate to scene when button is clicked', () => {
    const $route = {
      path: '/town'
    }

    wrapper.find('button').trigger('click')

    wrapper.vm.goto(false)

    expect(mutations.setMayorSelected).toBeTruthy()
    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
