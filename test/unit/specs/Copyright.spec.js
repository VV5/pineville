import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Copyright from '@/components/Copyright'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Copyright', () => {
  let wrapper
  let router

  beforeEach(() => {
    router = new VueRouter()

    wrapper = shallowMount(Copyright, {
      router, localVue
    })
  })

  test('should navigate to Introduction component when video has ended playing', () => {
    const $route = { path: '/introduction' }

    wrapper.find('video').trigger('ended')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
