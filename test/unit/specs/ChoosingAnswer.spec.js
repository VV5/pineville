import { shallowMount } from '@vue/test-utils'
import ChoosingAnswer from '@/components/ChoosingAnswer'

describe('ChoosingAnswer', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(ChoosingAnswer, {
      propsData: {
        choice: {}
      }
    })
  })

  test('', () => {
    wrapper.find('.choice').trigger('click')

    wrapper.vm.$emit('setActive')

    expect(wrapper.emitted().setActive).toBeTruthy()
  })
})
