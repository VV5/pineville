import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Consequence from '@/components/Consequence'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Consequence', () => {
  let wrapper
  let store, state, mutations
  let router

  beforeEach(() => {
    state = {
      towns: [
        { name: 'Foo' },
        { name: 'Bar' }
      ],
      consequences: [
        { name: 'Good', threshold: 75 },
        { name: 'Bad', threshold: 0 }
      ],
      totalScore: 0
    }

    mutations = {
      resetSelectedScene: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations
    })

    router = new VueRouter()

    wrapper = shallowMount(Consequence, {
      store, router, localVue
    })
  })

  test('should navigate back to scene and reset everything', () => {
    const $route = {
      path: '/town'
    }

    wrapper.find('.try-again').trigger('click')

    expect(mutations.resetSelectedScene).toHaveBeenCalled()
    expect(wrapper.vm.$route.path).toBe($route.path)
  })

  test('should navigate to credits page when button is clicked', () => {
      const $route = {
        path: '/credits'
      }

      wrapper.find('.goto-credits').trigger('click')

      expect(wrapper.vm.$route.path).toBe($route.path)
    }
  )
})
