# Changelog

## v1.3.0
- Added components for Copyright and Credits
- Updated audio assets
- Fixed text overflowing issue in modals 

## v1.2.1
- Changed router navigation behaviour
- Fixed background music not playing after restart
- Updated packages for `vue-loader` and `@vue/test-utils`

## v1.2.0
- Fixed responsiveness
- Added Font Awesome
- Fixed text wrap on mobile screens
- Added a function to enable residents only after speaking to the mayor first
- Optimised webpack configuration for production

## v1.1.1
- Fixed an error computing score

## v1.1.0
- Upgraded styling for pages
- Modified routes config to use ESM imports
- Fixed opening of site locally via `index.html`
- Upgraded to Webpack v4
